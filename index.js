/**
 * @format
 */
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';

//import App from './src/login/App';
//import App from './src/state_props/Home';
//import App from './src/state_props/State';
//import App from './src/screen/App';
//import App from './src/listview/FlatListHorizontal';
//import App from './src/listview/FlatListVertical';
import App from './src/crud_firebase/App';

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);

//tắt thông báo lỗi
console.disableYellowBox=true;
