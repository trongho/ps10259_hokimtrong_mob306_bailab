import React, { Component } from 'react';
import { 
    View, Text, TouchableOpacity, TextInput,FlatList, StyleSheet 
} from 'react-native';
import database from '@react-native-firebase/database';

function Item({ data }) {
    return (
      <View >
        <Text >{data.name}</Text>
        <Text >{data.age}</Text>
      </View>
    );
  }
  
export default class App extends Component{
    state = {
        dataUsers: null,
        name:'',
        age:'',
        currId:''
    }
    componentDidMount(){
        this.readUsers();
    }
    async readUsers() {
        // Create a reference
        const ref = database().ref('/users');
       
        // Fetch the data snapshot
        await ref.on('value',(snapshot)=>{
            this.setState({dataUsers:Object.entries(snapshot.val())});
            // this.setState({dataUsers:snapshot.toJSON()});
            
        });
        const snapshot = await ref.once('value');
        console.log('User data: ', Object.entries(snapshot.val()));

        
      }
      async addUser() {
        // Create a reference
        const ref = database().ref('/users');
       
        await ref.push({
          name: this.state.name,
          age: this.state.age,
        });
      }
      async updateUser(){
        var id = this.state.currId;
        console.log("currId: " + id);
        const ref = database().ref('/users/' + id);
        await ref.set({
            name:this.state.name,
            age:this.state.age
        });
        this.setState({name:'',age:'',currId:''});
      }
      async deleteUser(){
        var id = this.state.currId;
        console.log("currId: " + id);
        const ref = database().ref('/users/' + id);
        await ref.remove();
        this.setState({name:'',age:'',currId:''});
      }
    render(){
        return(
            <View>
                <TextInput style={styles.textInput} 
                onChangeText={(text) => this.setState({name:text})}
                value={this.state.name}/>
                <TextInput style={styles.textInput}
                onChangeText={(text) => this.setState({age:text})}
                value={this.state.age}/>
                <TouchableOpacity onPress={()=>this.addUser()}>
                    <Text>Add User</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.updateUser()}>
                    <Text>Update User</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.deleteUser()}>
                    <Text>Delete User</Text>
                </TouchableOpacity>
                <Text>Danh sách users</Text>
                <FlatList
                    data={this.state.dataUsers}
                    renderItem={({ item }) => 
                    (
                    <TouchableOpacity onPress={()=>this.setState({
                        name:item[1].name,age:item[1].age,currId:item[0]
                    })}>
                    <View >
                        <Text >{item[1].name}</Text>
                        <Text >{item[1].age}</Text>
                    </View>
                    </TouchableOpacity>
                    )}
                    keyExtractor={item => item.key}
                />
                {/* {this.state.dataUsers && 
                this.state.dataUsers.map((item,index) => 
                <View>
                    <Text>{item.name}</Text>
                    <Text>{item.age}</Text>
                </View>)} */}

                
            </View>
        )
    }
}
const styles = StyleSheet.create({
    "textInput":
    {   borderColor:'gray',
        borderWidth:1
    },
})