import React, { Component } from 'react'
import { View, Button, Alert } from 'react-native'

class DemoState extends Component {
    state = {
        count: 1
    }
    updateState = () => this.setState({ count:this.state.count + 1 })
    render() {
        return (
            <View>
                <Button onPress={this.updateState}
                    title={this.state.count + ''}
                />
            </View>
        );
    }
}

export default DemoState;
